---
title: "How To Get An Offline Copy Of The Arch Wiki"
image: images/thumbs/0591.jpg
date: 2020-04-07T12:23:40+06:00
author: Derek Taylor
tags: ["Arch Linux", "terminal", "command line"]
---

#### VIDEO

{{< amazon src="How+To+Get+An+Offline+Copy+Of+The+Arch+Wiki.mp4" >}}
&nbsp;

#### SHOW NOTES

Have you ever wanted an offline copy of the Arch Wiki on your local machine so that you always have the Wiki handy, even if you are not connected to the Internet?  Well, it's easy.  Download the following two packages from the standard Arch repos...

REFERENCED:
+ https://www.archlinux.org/packages/community/any/arch-wiki-docs/
+ https://www.archlinux.org/packages/community/any/arch-wiki-lite/