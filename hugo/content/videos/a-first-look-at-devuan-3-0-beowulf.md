---
title: "A First Look At Devuan 3.0 'Beowulf'"
image: images/thumbs/0638.jpg
date: 2020-06-03T12:23:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "Devuan"]
---

#### VIDEO

{{< amazon src="A+First+Look+At+Devuan+3+0+Beowulf.mp4" >}}
&nbsp;

#### SHOW NOTES

Devuan is a "protest distro" that is a fork of Debian but without systemd.  I've never looked at Devuan before, so I downloaded their standard desktop-live ISO and run through a quick installation.  It comes with the XFCE desktop and sysvinit, but there are other options for desktop environments and init systems if you so choose.

REFERENCED:
+ https://devuan.org/os/announce/beowulf-stable-announce-060120