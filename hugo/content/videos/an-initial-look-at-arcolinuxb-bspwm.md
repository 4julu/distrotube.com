---
title: "An Initial Look at ArcoLinuxB Bspwm Edition"
image: images/thumbs/0245.jpg
date: Tue, 17 Jul 2018 00:26:55 +0000
author: Derek Taylor
tags: ["Distro Reviews", "ArcoLinuxB", "bspwm"]
---

#### VIDEO

{{< amazon src="An+Initial+Look+at+ArcoLinuxB+Bspwm+Edition.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking an initial look at ArcoLinuxB Bspwm. https://arcolinux.info/
