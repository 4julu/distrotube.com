---
title: "Taking Into Account, Ep. 26 - Apt Bug, Ubuntu Core, Chromebooks, SUSE on ARM, MS phones"
image: images/thumbs/0340.jpg
date: Thu, 24 Jan 2019 22:35:52 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+26.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=ri5s0p_homs&amp;t=45s">0:45  Nasty security bug found in apt package manager. 

<a href="https://www.youtube.com/watch?v=ri5s0p_homs&amp;t=325s">5:25 Canonical brings Ubuntu 18.04 LTS to IoT and embedded devices with Ubuntu Core 18. 

<a href="https://www.youtube.com/watch?v=ri5s0p_homs&amp;t=546s">9:06 Chromebook power user? Get more out of your Chromebook by installing these Linux apps. 

<a href="https://www.youtube.com/watch?v=ri5s0p_homs&amp;t=900s">15:00 SUSE releases enterprise Linux for all major ARM processors. 

<a href="https://www.youtube.com/watch?v=ri5s0p_homs&amp;t=1106s">18:26 Microsoft declares the official end to its smartphone era.  Tells users to move to iOS or Android. 

<a href="https://www.youtube.com/watch?v=ri5s0p_homs&amp;t=1371s">22:51 I answer a viewer email about BSD, xmonad and qtile. 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=aIxgjy2hVzOOykY7aH35RUaiXKF8MTU1MzYzOTgzNUAxNTUzNTUzNDM1&amp;event=video_description&amp;v=ri5s0p_homs&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fnasty-security-bug-found-and-fixed-in-linux-apt%2F" target="_blank">https://www.zdnet.com/article/nasty-s...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=aIxgjy2hVzOOykY7aH35RUaiXKF8MTU1MzYzOTgzNUAxNTUzNTUzNDM1&amp;event=video_description&amp;v=ri5s0p_homs&amp;q=https%3A%2F%2Fnews.softpedia.com%2Fnews%2Fcanonical-brings-ubuntu-18-04-lts-to-iot-embedded-devices-with-ubuntu-core-18-524643.shtml" target="_blank">https://news.softpedia.com/news/canon...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=aIxgjy2hVzOOykY7aH35RUaiXKF8MTU1MzYzOTgzNUAxNTUzNTUzNDM1&amp;event=video_description&amp;v=ri5s0p_homs&amp;q=https%3A%2F%2Fwww.computerworld.com%2Farticle%2F3328838%2Fchrome-os%2Fbest-linux-apps-for-chromebooks.html" target="_blank">https://www.computerworld.com/article...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=aIxgjy2hVzOOykY7aH35RUaiXKF8MTU1MzYzOTgzNUAxNTUzNTUzNDM1&amp;event=video_description&amp;v=ri5s0p_homs&amp;q=https%3A%2F%2Fwww.networkworld.com%2Farticle%2F3334781%2Flinux%2Fsuse-releases-enterprise-linux-for-all-major-arm-processors.html" target="_blank">https://www.networkworld.com/article/...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=aIxgjy2hVzOOykY7aH35RUaiXKF8MTU1MzYzOTgzNUAxNTUzNTUzNDM1&amp;event=video_description&amp;v=ri5s0p_homs&amp;q=https%3A%2F%2Fwww.computerworld.com%2Farticle%2F3334963%2Fmicrosoft-windows%2Fmicrosoft-declares-the-official-end-to-its-smartphone-era.html" target="_blank">https://www.computerworld.com/article...---
