---
title: "MX 17.1 Installation and Review"
image: images/thumbs/0147.jpg
date: Sat, 17 Mar 2018 21:40:13 +0000
author: Derek Taylor
tags: ["Distro Reviews", "MX Linux", "XFCE"]
---

#### VIDEO

{{< amazon src="MX+17.1+Installation+and+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

The new MX 17.1 was recently released so I had to give it a quick look. MX is a Debian based Linux distro that uses the XFCE desktop and comes with a number of custom built MX utilities. <a href="https://mxlinux.org/">https://mxlinux.org/</a>
