---
title: "Elementary OS 5.1 'Hera' - Imaginative and Visionary"
image: images/thumbs/0476.jpg
date: Sun, 08 Dec 2019 00:20:00 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Elementary OS"]
---

#### VIDEO

{{< amazon src="Elementary+OS+51+Hera+-+Imaginative+and+Visionary+.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm taking a quick a look at the newly released Elementary OS 5.1, codenamed "Hera", as I install it on one of my testing laptops and in a virtual machine.  I could argue that this might be the best desktop operating system on the planet right now.  
	
#### REFERENCED:
+ ► https://blog.elementary.io/introducing-elementary-os-5-1-hera/
