---
title: "Nested X Sessions With Xephyr"
image: images/thumbs/0521.jpg
date: 2020-01-21T12:22:40+06:00
author: Derek Taylor
tags: ["Tiling Window Managers", ""]
---

#### VIDEO

{{< amazon src="Nested+X+Sessions+With+Xephyr.mp4" >}}
&nbsp;

#### SHOW NOTES

This is a quick video in response to a viewer question.  That question is, "How do you test your tiling window manager configuration?"  A way to do this is by using an X11 program called Xephyr, which creates a nested X session.

REFERENCED:
+ https://www.x.org/archive/X11R7.5/doc/man/man1/Xephyr.1.html
+ https://wiki.archlinux.org/index.php/Xephyr