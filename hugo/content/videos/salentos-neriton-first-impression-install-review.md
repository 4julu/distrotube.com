---
title: "SalentOS Neriton First Impression Install & Review"
image: images/thumbs/0067.jpg
date: Mon, 18 Dec 2017 15:26:26 +0000
author: Derek Taylor
tags: ["Distro Reviews", "SalentOS", "Openbox"]
---

#### VIDEO

{{< amazon src="SalentOS+Neriton+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

I take a quick look at SalentOS, a Linux distro based on Debian (stable) and featuring the Openbox window manager. Light on resources, attractive design and sensible choices for software make this distro worth checking out. <a href="http://www.salentos.it/home.html">http://www.salentos.it/home.html</a>
