---
title: "Espanso Is A Cross Platform Text Expander"
image: images/thumbs/0477.jpg
date: Wed, 11 Dec 2019 00:20:00 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Espanso+Is+A+Cross+Platform+Text+Expander.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm taking a quick a look at nice text expander called Espanso, which is a cross-platform program written in Rust.  It's very easy to install and customize to your liking,
	
#### REFERENCED:
+ ► https://espanso.org/
+ ► https://github.com/federico-terzi/espanso
