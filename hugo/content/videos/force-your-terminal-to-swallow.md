---
title: "Force Your Terminal To Swallow"
image: images/thumbs/0650.jpg
date: 2020-06-20T12:23:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Force+Your+Terminal+To+Swallow.mp4" >}}
&nbsp;

#### SHOW NOTES

I have been bombarded with questions regarding "window swallowing" recently.  I'm getting messaged on Mastodon and Reddit and through email.  I thought I had better answer this question before my inbox explodes.

REFERENCED:
+ https://github.com/baskerville/xdo - Xdo
+ https://github.com/salman-abedin/devour/ - Devour
