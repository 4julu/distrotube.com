---
title: "Trayer - A Lightweight System Tray For Your Linux Desktop"
image: images/thumbs/0367.jpg
date: Tue, 12 Mar 2019 23:16:22 +0000
author: Derek Taylor
tags: ["GUI Apps", ""]
---

#### VIDEO

{{< amazon src="Trayer+-+A+Lightweight+System+Tray+For+Your+Linux+Desktop.mp4" >}}
&nbsp;

#### SHOW NOTES

A viewer recently asked me about system trays.  How do you get a system  tray on a desktop and/or panel that does not include one out-of-the-box.   Today, I'm going to share with you guys a neat little program called trayer. 

📰 REFERENCED: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgithub.com%2Fsargon%2Ftrayer-srg&amp;redir_token=6ExLbCjH0qL_9m1ojuzBkWzFYxV8MTU1MzY0MjE3N0AxNTUzNTU1Nzc3&amp;v=MyJjiYVggBs&amp;event=video_description" target="_blank">https://github.com/sargon/trayer-srg
