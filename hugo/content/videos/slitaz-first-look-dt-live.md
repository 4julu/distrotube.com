---
title: "SliTaz GNU/Linux First Look - DT LIVE"
image: images/thumbs/0225.jpg
date: Wed, 06 Jun 2018 23:51:55 +0000
author: Derek Taylor
tags: ["Distro Reviews", "SliTaz", "Live Stream"]
---

#### VIDEO

{{< amazon src="SliTaz+GNULinux+First+Look+-+DT+LIVE.mp4" >}}
&nbsp;

#### SHOW NOTES

Tonight, I'm going to take a quick first look at SliTaz. This independent distro has been requested by a few viewers. It uses the Openbox window manager which, of course, I love! http://www.slitaz.org/
