---
title: "A Command Line Pastebin and a Terminal Session Recorder - Termbin & Asciinema"
image: images/thumbs/0363.jpg
date: Sat, 09 Mar 2019 23:14:04 +0000
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="A+Command+Line+Pastebin+and+a+Terminal+Session+Recorder+-+Termbin+%26+Asciinema.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm sharing with you a neat command line "pastebin" alternative  called termbin , as well a terminal session recorder with a horrible  name--asciinema! 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=http%3A%2F%2Ftermbin.com%2F&amp;event=video_description&amp;v=nAUohhEJ56M&amp;redir_token=0AKyBcFwA1FB6IPANudVhh9Jyet8MTU1MzY0MjA3MkAxNTUzNTU1Njcy" target="_blank">http://termbin.com/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fasciinema.org%2F&amp;event=video_description&amp;v=nAUohhEJ56M&amp;redir_token=0AKyBcFwA1FB6IPANudVhh9Jyet8MTU1MzY0MjA3MkAxNTUzNTU1Njcy" target="_blank">https://asciinema.org/
