---
title: "Slackware Package Management With Serge"
image: images/thumbs/0267.jpg
date: Fri, 24 Aug 2018 17:55:38 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Slackware"]
---

#### VIDEO

{{< amazon src="Slackware+Package+Management+With+Serge.mp4" >}}
&nbsp;

#### SHOW NOTES

This video is a follow-up to the first video I did with Serge ( <a href="https://www.youtube.com/channel/UCkNqc5_jT_ORnxWGlPosA5g">https://www.youtube.com/channel/UCkNq... ) on Slackware: <a href="https://www.youtube.com/watch?v=naHiSS9nfTY">https://www.youtube.com/watch?v=naHiS... 

In this video, Serge walks me through some of the package management  tools available in Slackware.  We install some of my favorite programs:  leafpad, ranger, deadbeef, xfe and clementine. 

Building a package the "traditional way": <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=AEl6_hJZLug&amp;redir_token=i8N9-Zss0pqgkYRqB58g65Q432F8MTU1MzQ0OTk1NUAxNTUzMzYzNTU1&amp;q=https%3A%2F%2Fwww.slackwiki.com%2FBuilding_A_Package" target="_blank">https://www.slackwiki.com/Building_A_... 

Variables for installation directories: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=AEl6_hJZLug&amp;redir_token=i8N9-Zss0pqgkYRqB58g65Q432F8MTU1MzQ0OTk1NUAxNTUzMzYzNTU1&amp;q=https%3A%2F%2Fwww.gnu.org%2Fprep%2Fstandards%2Fhtml_node%2FDirectory-Variables.html" target="_blank">https://www.gnu.org/prep/standards/ht... 

SlackBuilds <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=AEl6_hJZLug&amp;redir_token=i8N9-Zss0pqgkYRqB58g65Q432F8MTU1MzQ0OTk1NUAxNTUzMzYzNTU1&amp;q=https%3A%2F%2Fwww.slackbuilds.org" target="_blank">https://www.slackbuilds.org Sbopkg: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=AEl6_hJZLug&amp;redir_token=i8N9-Zss0pqgkYRqB58g65Q432F8MTU1MzQ0OTk1NUAxNTUzMzYzNTU1&amp;q=https%3A%2F%2Fsbopkg.org%2Findex.php" target="_blank">https://sbopkg.org/index.php 

Sbotools: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=AEl6_hJZLug&amp;redir_token=i8N9-Zss0pqgkYRqB58g65Q432F8MTU1MzQ0OTk1NUAxNTUzMzYzNTU1&amp;q=https%3A%2F%2Fpink-mist.github.io%2Fsbotools%2F" target="_blank">https://pink-mist.github.io/sbotools/ 

To fix the errors with sbotools: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=AEl6_hJZLug&amp;redir_token=i8N9-Zss0pqgkYRqB58g65Q432F8MTU1MzQ0OTk1NUAxNTUzMzYzNTU1&amp;q=https%3A%2F%2Fgithub.com%2Fpink-mist%2Fsbotools%2Fissues%2F75" target="_blank">https://github.com/pink-mist/sbotools...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=AEl6_hJZLug&amp;redir_token=i8N9-Zss0pqgkYRqB58g65Q432F8MTU1MzQ0OTk1NUAxNTUzMzYzNTU1&amp;q=https%3A%2F%2Fgithub.com%2Fyukiisbored%2Fsbotools%2Fcommit%2Fb5058e534e76e9bda53ccf4019b1b1d2da6469f3" target="_blank">https://github.com/yukiisbored/sbotoo... 

Donate to Slackware developer Patrick Volkerding: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=AEl6_hJZLug&amp;redir_token=i8N9-Zss0pqgkYRqB58g65Q432F8MTU1MzQ0OTk1NUAxNTUzMzYzNTU1&amp;q=https%3A%2F%2Fwww.paypal.me%2Fvolkerdi" target="_blank">https://www.paypal.me/volkerdi 

ERRATUM: In the video, Serge mentions that sbopkg cannot solve dependencies which  is correct but incomplete.  In recent versions, they have added a tool  called sqg ( <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=AEl6_hJZLug&amp;redir_token=i8N9-Zss0pqgkYRqB58g65Q432F8MTU1MzQ0OTk1NUAxNTUzMzYzNTU1&amp;q=https%3A%2F%2Fsbopkg.org%2Fqueues.php" target="_blank">https://sbopkg.org/queues.php )
