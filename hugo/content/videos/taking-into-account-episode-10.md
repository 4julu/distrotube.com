---
title: "Taking Into Account, Ep. 10 - CoC Myths, Chrome Privacy, Firefox Monitor, Mint, Fedora, Vivaldi"
image: images/thumbs/0290.jpg
date: Thu, 27 Sep 2018 19:48:15 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account%2C+Ep.+10+-+CoC+Myths%2C+Chrome+Privacy%2C+Firefox+Monitor%2C+Mint%2C+Fedora%2C+Vivaldi.mp4" >}}
&nbsp;

#### SHOW NOTES

<a href="https://www.youtube.com/watch?v=3kHVVxBT6kQ&amp;t=43s">0:43  Myths about Linus Torvalds leaving and the CoC debunked. 

<a href="https://www.youtube.com/watch?v=3kHVVxBT6kQ&amp;t=658s">10:58 Google Chrome's forced login feature violates user privacy. 

<a href="https://www.youtube.com/watch?v=3kHVVxBT6kQ&amp;t=886s">14:46 Firefox Monitor helps people learn if they've been part of a data breach. 

<a href="https://www.youtube.com/watch?v=3kHVVxBT6kQ&amp;t=1139s">18:59 Twenty-two things to do after installing Linux Mint.  Seriously? 

<a href="https://www.youtube.com/watch?v=3kHVVxBT6kQ&amp;t=1649s">27:29 New releases - Fedora 29 Beta, WLinux and Vivaldi 2.0. 

<a href="https://www.youtube.com/watch?v=3kHVVxBT6kQ&amp;t=1926s">32:06 A quick announcement - I'm taking a short break from the channel. 

REFERENCED IN THE VIDEO: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hZqKrDJanJ1lY9pa5ADyXoHFtKV8MTU1MzQ1NjkxMUAxNTUzMzcwNTEx&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Flinus-torvalds-and-linux-code-of-conduct-myths%2F&amp;v=3kHVVxBT6kQ&amp;event=video_description" target="_blank">https://www.zdnet.com/article/linus-t...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hZqKrDJanJ1lY9pa5ADyXoHFtKV8MTU1MzQ1NjkxMUAxNTUzMzcwNTEx&amp;q=https%3A%2F%2Flulz.com%2Flinux-devs-threaten-killswitch-coc-controversy-1252%2F&amp;v=3kHVVxBT6kQ&amp;event=video_description" target="_blank">https://lulz.com/linux-devs-threaten-...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hZqKrDJanJ1lY9pa5ADyXoHFtKV8MTU1MzQ1NjkxMUAxNTUzMzcwNTEx&amp;q=http%3A%2F%2Fwww.groklaw.net%2Farticle.php%3Fstory%3D2006062204552163&amp;v=3kHVVxBT6kQ&amp;event=video_description" target="_blank">http://www.groklaw.net/article.php?st...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hZqKrDJanJ1lY9pa5ADyXoHFtKV8MTU1MzQ1NjkxMUAxNTUzMzcwNTEx&amp;q=https%3A%2F%2Fwww.reddit.com%2Fr%2Flinuxmasterrace%2Fcomments%2F9iw8d3%2Fidea_what_if_richard_stallman_and_his_lieutenants%2F&amp;v=3kHVVxBT6kQ&amp;event=video_description" target="_blank">https://www.reddit.com/r/linuxmasterr...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hZqKrDJanJ1lY9pa5ADyXoHFtKV8MTU1MzQ1NjkxMUAxNTUzMzcwNTEx&amp;q=https%3A%2F%2Ftechcrunch.com%2F2018%2F09%2F24%2Fsecurity-experts-say-chrome-69s-forced-login-feature-violates-user-privacy%2F&amp;v=3kHVVxBT6kQ&amp;event=video_description" target="_blank">https://techcrunch.com/2018/09/24/sec...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hZqKrDJanJ1lY9pa5ADyXoHFtKV8MTU1MzQ1NjkxMUAxNTUzMzcwNTEx&amp;q=https%3A%2F%2Ftechcrunch.com%2F2018%2F09%2F26%2Fgoogle-to-give-chrome-users-an-opt-out-to-forced-login-after-privacy-backlash%2F&amp;v=3kHVVxBT6kQ&amp;event=video_description" target="_blank">https://techcrunch.com/2018/09/26/goo...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hZqKrDJanJ1lY9pa5ADyXoHFtKV8MTU1MzQ1NjkxMUAxNTUzMzcwNTEx&amp;q=https%3A%2F%2Fblog.mozilla.org%2Fblog%2F2018%2F09%2F25%2Fintroducing-firefox-monitor-helping-people-take-control-after-a-data-breach%2F&amp;v=3kHVVxBT6kQ&amp;event=video_description" target="_blank">https://blog.mozilla.org/blog/2018/09...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hZqKrDJanJ1lY9pa5ADyXoHFtKV8MTU1MzQ1NjkxMUAxNTUzMzcwNTEx&amp;q=https%3A%2F%2Fblog.mozilla.org%2Fsecurity%2F2018%2F06%2F25%2Fscanning-breached-accounts-k-anonymity%2F&amp;v=3kHVVxBT6kQ&amp;event=video_description" target="_blank">https://blog.mozilla.org/security/201...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hZqKrDJanJ1lY9pa5ADyXoHFtKV8MTU1MzQ1NjkxMUAxNTUzMzcwNTEx&amp;q=https%3A%2F%2Ffossbytes.com%2Fthings-to-do-after-installing-linux-mint%2F&amp;v=3kHVVxBT6kQ&amp;event=video_description" target="_blank">https://fossbytes.com/things-to-do-af...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hZqKrDJanJ1lY9pa5ADyXoHFtKV8MTU1MzQ1NjkxMUAxNTUzMzcwNTEx&amp;q=https%3A%2F%2Fwww.redhat.com%2Fen%2Fblog%2Ffedora-29-beta-now-available&amp;v=3kHVVxBT6kQ&amp;event=video_description" target="_blank">https://www.redhat.com/en/blog/fedora...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hZqKrDJanJ1lY9pa5ADyXoHFtKV8MTU1MzQ1NjkxMUAxNTUzMzcwNTEx&amp;q=https%3A%2F%2Fmspoweruser.com%2Fnew-linux-distro-specifically-designed-for-windows-comes-to-the-microsoft-store%2F&amp;v=3kHVVxBT6kQ&amp;event=video_description" target="_blank">https://mspoweruser.com/new-linux-dis...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=hZqKrDJanJ1lY9pa5ADyXoHFtKV8MTU1MzQ1NjkxMUAxNTUzMzcwNTEx&amp;q=https%3A%2F%2Fvivaldi.com%2Fblog%2Fvivaldi-2-0-your-browser-matters%2F&amp;v=3kHVVxBT6kQ&amp;event=video_description" target="_blank">https://vivaldi.com/blog/vivaldi-2-0-...
