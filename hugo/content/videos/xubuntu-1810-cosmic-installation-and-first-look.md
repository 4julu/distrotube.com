---
title: "Xubuntu 18.10 'Cosmic Cuttlefish' Installation and First Look"
image: images/thumbs/0297.jpg
date: Wed, 17 Oct 2018 23:56:52 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Xubuntu"]
---

#### VIDEO

{{< amazon src="Xubuntu+1810+Cosmic+Cuttlefish+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking a quick look at Xubuntu 18.10 "Cosmic Cuttlefish".  
Xubuntu features the Xfce (sometimes pronounced as "X-Face") desktop 
environment and nice suite of programs.  Xubuntu 18.10 will be released 
"officially" tomorrow so technically this daily build that I'm looking 
at is beta, but nothing major should change between now and tomorrow.

<a href="https://www.youtube.com/redirect?redir_token=Zxv0kVZDI5RsMxwv_tLNYUlILdd8MTU1MzQ3MTgxOEAxNTUzMzg1NDE4&amp;v=qqkMY4jN7Sw&amp;q=https%3A%2F%2Fxubuntu.org%2F&amp;event=video_description" rel="noreferrer noopener" target="_blank">https://xubuntu.org/
