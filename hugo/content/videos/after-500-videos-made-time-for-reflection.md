---
title: "After 500 Videos Made, A Time For Reflection"
image: images/thumbs/0426.jpg
date: Sat, 27 Jul 2019 05:49:00 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="After+500+Videos+Made+A+Time+For+Reflection.mp4" >}}
&nbsp;

#### SHOW NOTES

Tomorrow I plan to do a live stream to mark my 500th video on YouTube. What a ride! What a journey! I want to thank all you guys for your support. I am honored to call each and every one of you, my Linux brothers and sisters.
