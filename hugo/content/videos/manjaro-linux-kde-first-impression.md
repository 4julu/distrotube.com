---
title: "Manjaro Linux KDE First Impression Install & Review"
date: Sun, 08 Oct 2017 01:27:36 +0000
image: images/thumbs/0001.jpg
author: Derek Taylor
tags: ["Distro Reviews", "Manjaro", "KDE"]
---

#### VIDEO  

{{< amazon src="Manjaro+Linux+KDE+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

Today I take a first look at Manjaro Linux KDE Edition. I've been hearing a lot of good things about Manjaro but have never installed it before. So this will be quick install and first impressions review of Manjaro KDE. I will keep this installation of Manjaro for awhile and will revisit it in future videos. So stay tuned!
