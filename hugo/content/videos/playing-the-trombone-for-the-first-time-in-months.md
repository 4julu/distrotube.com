---
title: "Playing The Trombone For The First Time In Months"
image: images/thumbs/0475.jpg
date: Fri, 06 Dec 2019 00:20:00 +0000
author: Derek Taylor
tags: ["Audio", ""]
---

#### VIDEO

{{< amazon src="Playing+The+Trombone+For+The+First+Time+In+Months.mp4" >}}
&nbsp;

#### SHOW NOTES

One of the things some of the long-time followers of the channel have asked is for me to share more recordings of me playing the trombone.  Full disclosure: I haven't pulled out the trombone, or any other musical instrument, in months.  But I think it's time I get back into "playing" shape.  So I'm sharing with you guys the very first notes and admittedly, this sounds pretty rough.  And just playing a couple of minutes was quite tiring on the facial muscles.  I'd say I need 2-3 weeks to get into real shape. 

Also, this provides me a way to also test out my microphones and audio equipment on instrumental recording rather than just spoken voice.  This was recorded on the Blue Ember mic plugged into the Scarlett 2i2 interface.  Gain was set very low since the trombone is so loud in such a small room.
	
#### REFERENCED:
+ ► Blue Ember - https://amzn.to/2DLJRZh
+ ► Blue Baby Bottle SL - https://amzn.to/2RkZRcT
+ ► Scarlett 2i2 Interface - https://amzn.to/2YjLB5A
