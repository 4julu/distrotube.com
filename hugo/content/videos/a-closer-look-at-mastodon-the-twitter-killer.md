---
title: "A Closer Look at Mastodon, The Twitter Killer!"
image: images/thumbs/0261.jpg
date: Mon, 06 Aug 2018 19:20:46 +0000
author: Derek Taylor
tags: ["Social Media", "Mastodon"]
---

#### VIDEO

{{< amazon src="A+Closer+Look+at+Mastodon%2C+The+Twitter+Killer!.mp4" >}}
&nbsp;

#### SHOW NOTES

In recent weeks, I've been touting Mastodon and asking you guys to join 
me over there.  And many of you have signed up!  And many of you have 
asked me how exactly do you use Mastodon.  So here's a quick tutorial.

<a href="https://www.youtube.com/redirect?q=https%3A%2F%2Fjoinmastodon.org%2F&amp;event=video_description&amp;v=wGJW34aVmwE&amp;redir_token=la8Ze9aFIq1u6Glk4XVt-_btfzF8MTU1MzQ1NTI4MkAxNTUzMzY4ODgy" rel="noreferrer noopener" target="_blank">https://joinmastodon.org/
