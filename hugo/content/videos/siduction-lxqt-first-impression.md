---
title: "Siduction LXQt First Impression Install & Review"
image: images/thumbs/0008.jpg
date: Thu, 12 Oct 2017 01:47:03 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Siduction", "LXQt"]
---

#### VIDEO

{{< amazon src="Siduction+LXQt+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

I install yet another rolling release distro that I've never tried before -- Siduction! I give my first impression review of the install process for Siduction as well as my first impression of the LXQt desktop. Similar to what I am doing with the other rolling release distros I've reviewed recently, I will keep Siduction up-to-date and do further videos on my experience with Siduction in the coming weeks/months.
