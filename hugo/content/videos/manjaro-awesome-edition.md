---
title: "Manjaro Awesome Edition"
image: images/thumbs/0447.jpg
date: Thu, 10 Oct 2019 23:54:00 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Manjaro", "awesome"]
---

#### VIDEO

{{< amazon src="Manjaro+Awesome+Edition.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm taking a quick look at one of Manjaro's community editions--their Awesome WM edition! One problem though. It doesn't look like there is a recent ISO, so this could be dicey.

#### REFERENCED:
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=TO2HoJ1f4Ig&amp;event=video_description&amp;q=https%3A%2F%2Fmanjaro.org%2F&amp;redir_token=vss54k4KpY6rQxwqzQEV_KWyCxV8MTU3NzQ5MDg4MkAxNTc3NDA0NDgy" target="_blank" rel="nofollow noopener noreferrer">https://manjaro.org/
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=TO2HoJ1f4Ig&amp;event=video_description&amp;q=https%3A%2F%2Fforum.manjaro.org%2Ft%2Fmanjaro-awesome-18-1-0-rc4-iso-testing-branch%2F95302&amp;redir_token=vss54k4KpY6rQxwqzQEV_KWyCxV8MTU3NzQ5MDg4MkAxNTc3NDA0NDgy" target="_blank" rel="nofollow noopener noreferrer">https://forum.manjaro.org/t/manjaro-a...
