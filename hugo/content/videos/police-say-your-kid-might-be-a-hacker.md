---
title: "Police Say Your Kid Might Be A Hacker If He Uses..."
image: images/thumbs/0545.jpg
date: 2020-02-15T12:22:40+06:00
author: Derek Taylor
tags: ["FOSS Philosophy", ""]
---

#### VIDEO

{{< amazon src="Police+Say+Your+Kid+Might+Be+A+Hacker+If+He+Uses.mp4" >}}
&nbsp;

#### SHOW NOTES

A UK police department recently put out a poster listing the tell-tale signs that your child might be hacking.  They list software that, if you see your child using, you should report them to the police.  

REFERENCED:
+  https://www.eff.org/ - Donate to the EFF
+  https://www.fsf.org/ - Donate to the FSF