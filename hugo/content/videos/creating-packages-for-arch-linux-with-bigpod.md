---
title: "Creating Packages For Arch Linux With Help From Bigpod"
image: images/thumbs/0571.jpg
date: 2020-03-16T12:22:40+06:00
author: Derek Taylor
tags: ["Arch Linux", ""]
---

#### VIDEO

{{< amazon src="Creating+Packages+For+Arch+Linux+with+bigpod.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, Linux YouTuber and community member, Bigpod, will walk me (and you) through the process of how to create a package for Arch Linux.  These packages can be submitted to the AUR or used locally on your machines.

CHECK OUT BIGPOD:
+ https://www.youtube.com/user/bigpodgurc - Bigpod on YouTube
+ https://www.youtube.com/watch?v=h91Z9O2mxOU - Bigpod's Video on Creating Arch Repositories
+ https://lbry.tv/@bigpod:1 - Bigpod on LBRY

REFERENCED:
+ https://wiki.archlinux.org/index.php/creating_packages - Arch Wiki on Creating Packages
