---
title: "Switching To Bspwm - Initial Thoughts"
image: images/thumbs/0429.jpg
date: Fri, 09 Aug 2019 23:16:00 +0000
author: Derek Taylor
tags: ["tiling window managers", "bspwm"]
---

#### VIDEO

{{< amazon src="Switching+To+Bspwm+Initial+Thoughts.mp4" >}}
&nbsp;

#### SHOW NOTES

I recently made the switch to bspwm, a manual tiling window manager that is a bit of a strange beast. I'm just getting through some of the initial setup and I thought I would share my initial thoughts so far.


&nbsp;
#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=VKSxDWLLfn4&amp;event=video_description&amp;redir_token=zEC1txL6V5--plveslGkDRI9KR98MTU3NzQ4ODYwM0AxNTc3NDAyMjAz&amp;q=https%3A%2F%2Fgithub.com%2Fbaskerville%2Fbspwm" target="_blank" rel="nofollow noopener noreferrer">https://github.com/baskerville/bspwm
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=VKSxDWLLfn4&amp;event=video_description&amp;redir_token=zEC1txL6V5--plveslGkDRI9KR98MTU3NzQ4ODYwM0AxNTc3NDAyMjAz&amp;q=https%3A%2F%2Fgithub.com%2Fbaskerville%2Fsxhkd" target="_blank" rel="nofollow noopener noreferrer">https://github.com/baskerville/sxhkd
