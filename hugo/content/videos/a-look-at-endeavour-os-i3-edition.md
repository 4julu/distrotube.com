---
title: "A Look At EndeavourOS i3 Edition"
image: images/thumbs/0598.jpg
date: 2020-04-16T12:23:40+06:00
author: Derek Taylor
tags: ["EndeavourOS", "i3", "Tiling Window Managers"]
---

#### VIDEO

{{< amazon src="A+Look+At+EndeavourOS+i3+Edition.mp4" >}}
&nbsp;

#### SHOW NOTES

EndeavourOS is an Arch Linux-based distribution which features graphical install options and pre-configured desktop environments including a pre-configured i3, which I will take a look at. 

REFERENCED:
+ https://endeavouros.com/news/the-april-release-has-arrived/