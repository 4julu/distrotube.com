---
title: "Linux Mint 19 Cinnamon Installation and First Look"
image: images/thumbs/0238.jpg
date: Fri, 29 Jun 2018 00:17:06 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Linux Mint"]
---

#### VIDEO

{{< amazon src="Linux+Mint+19+Cinnamon+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking a quick first look at Linux Mint 19 Cinnamon.
