---
title: "Why All The Hate For Ubuntu?!"
image: images/thumbs/0542.jpg
date: 2020-02-12T12:22:40+06:00
author: Derek Taylor
tags: ["Community", "Ubuntu"]
---

#### VIDEO

{{< amazon src="Why+All+The+Hate+For+Ubuntu.mp4" >}}
&nbsp;

#### SHOW NOTES


Ubuntu, since it's very beginning, has always been the Linux distribution that everyone loves to hate.  Why all the hate though?  Is it justified or is it just mob mentality and people spouting the same nonsensical reasons?

REFERENCED:
+  https://ubuntu.com/