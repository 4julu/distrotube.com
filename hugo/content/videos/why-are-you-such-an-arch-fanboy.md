---
title: "Why Are You Such An Arch Fanboy? (Plus Other Questions Answered)"
image: images/thumbs/0639.jpg
date: 2020-06-04T12:23:40+06:00
author: Derek Taylor
tags: ["Arch Linux", ""]
---

#### VIDEO

{{< amazon src="Why+Am+I+Such+An+Arch+Fanboy+Plus+Other+Questions+Answered.mp4" >}}
&nbsp;

#### SHOW NOTES

In this lengthy rant video, I address a few questions that I've been receiving from viewers. Some of the topics include backing up your Linux system, managing your dotfiles, gaming on Windows, and why am I such an "Arch fanboy"?