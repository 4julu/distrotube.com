---
title: "I Install unity7sl and Discuss the Pros of the Unity Desktop and Its Future"
image: images/thumbs/0058.jpg
date: Tue, 12 Dec 2017 15:14:16 +0000
author: Derek Taylor
tags: ["Distro Reviews", "unity7sl"]
---

#### VIDEO

{{< amazon src="I+Install+unity7sl+and+Discuss+the+Pros+of+the+Unity+Desktop+and+Its+Future..mp4" >}} 
&nbsp;

#### SHOW NOTES

I stumbled across a random ISO on SourceFourge. Its just one guy's spin of Ubuntu 18.04 (development) with the Unity desktop rather than the Gnome desktop that Ubuntu 18.04 plans to ship with as default. I also discuss why I think Unity is a fantastic desktop environment and why it will probably continue to live.<a href="https://sourceforge.net/projects/unity7sl/">https://sourceforge.net/projects/unity7sl/</a>
