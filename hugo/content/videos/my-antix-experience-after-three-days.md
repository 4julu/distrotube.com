---
title: "My AntiX Experience After Three Days"
image: images/thumbs/0154.jpg
date: Thu, 22 Mar 2018 22:02:12 +0000
author: Derek Taylor
tags: ["Distro Reviews", "AntiX"]
---

#### VIDEO

{{< amazon src="My+AntiX+Experience+After+Three+Days.mp4" >}}  
&nbsp;

#### SHOW NOTES

It's been three days since I installed AntiX 17.1 on my main production machine. I haven't played around with it too much but here are some of my initial thoughts on AntiX. I also discuss a few of the programs that installed on AntiX.
