---
title: "OpenSUSE Leap 15 Installation and First Look"
image: images/thumbs/0218.jpg
date: Sun, 27 May 2018 23:38:57 +0000
author: Derek Taylor
tags: ["Distro Reviews", "OpenSUSE"]
---

#### VIDEO

{{< amazon src="OpenSUSE+Leap+15+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm taking the recently released OpenSUSE Leap 15 for a quick spin. I'm installing the Plasma version of Leap using the net installer. https://www.opensuse.org/
