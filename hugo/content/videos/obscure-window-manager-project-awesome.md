---
title: "Free and Open Source Games on Linux"
image: images/thumbs/0171.jpg
date: Sat, 07 Apr 2018 22:23:03 +0000
author: Derek Taylor
tags: ["Gaming"]
---

#### VIDEO

{{< amazon src="Free+and+Open+Source+Games+on+Linux.mp4" >}}
&nbsp;

#### SHOW NOTES

There is quite a selection of free and open source games available on Linux. This is just a quick overview of some of the games available. 
 Referenced in video: <a href="https://www.slant.co/topics/1933/~best-open-source-games">https://www.slant.co/topics/1933/~best-open-source-games</a>
