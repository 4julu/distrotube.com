---
title: "The Fast And Flexible Albert Launcher"
image: images/thumbs/0425.jpg
date: Sat, 27 Jul 2019 05:47:00 +0000
author: Derek Taylor
tags: ["GUI Apps", ""]
---

#### VIDEO

{{< amazon src="The+Fast+And+Flexible+Albert+Launcher.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I'm going to take a quick look at the Albert launcher. Albert is a fast and flexible run launcher that you can use to launch programs, search files, search the web, run shell commands, etc.


&nbsp;
#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?redir_token=nWnCb2-977FWS810BeRVm-BjF8V8MTU3NTY5NzY1NEAxNTc1NjExMjU0&amp;v=i95rn3sl_Uo&amp;q=https%3A%2F%2Fgithub.com%2Falbertlauncher%2Falbert&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://github.com/albertlauncher/albert
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?redir_token=nWnCb2-977FWS810BeRVm-BjF8V8MTU3NTY5NzY1NEAxNTc1NjExMjU0&amp;v=i95rn3sl_Uo&amp;q=https%3A%2F%2Falbertlauncher.github.io%2F&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://albertlauncher.github.io/
