---
title: "Pardus 17.4 Deepin Installation and First Look"
image: images/thumbs/0304.jpg
date: Sat, 10 Nov 2018 00:15:25 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Pardus"]
---

#### VIDEO

{{< amazon src="Pardus+17+4+Deepin+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I take a quick look at the recently released Pardus 17.4 
Deepin edition.  I've taken a look at Pardus before on the channel, and I
 had some issues with it.  Will this version be any better?

<a href="https://www.youtube.com/redirect?v=T63FdQaWFzI&amp;redir_token=81px6gmJ9BdRz7mtxLETeiZg-MB8MTU1MzQ3Mjk1MUAxNTUzMzg2NTUx&amp;q=https%3A%2F%2Fwww.pardus.org.tr%2F&amp;event=video_description" rel="noreferrer noopener" target="_blank">https://www.pardus.org.tr/
