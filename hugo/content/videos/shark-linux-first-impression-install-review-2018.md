---
title: "Shark Linux First Impression Install and Review"
image: images/thumbs/0159.jpg
date: Mon, 26 Mar 2018 22:07:56 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Shark Linux"]
---

#### VIDEO

{{< amazon src="Shark+Linux+First+Impression+Install+and+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this first impression install and review, I take a brief look at SharkLinux--an Ubuntu-based distro that comes with some interesting software and features. <a href="http://sharklinuxos.org/">http://sharklinuxos.org/</a>
