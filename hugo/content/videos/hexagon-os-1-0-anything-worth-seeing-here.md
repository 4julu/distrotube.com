---
title: "Hexagon OS 1.0 - Anything Worth Seeing Here?"
image: images/thumbs/0416.jpg
date: Sat, 29 Jun 2019 05:32:00 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Hexagon OS"]
---

#### VIDEO

{{< amazon src="Hexagon+OS+10+Anything+Worth+Seeing+Here.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking a quick look at Hexagon OS 1.0. This is an Ubuntu-based Linux distro that uses the Xfce Desktop Environment. Does Hexagon OS differentiate itself from the dozens of other Ubuntu-based distros out there?


&nbsp;
#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?event=video_description&amp;v=-en6P5hV0-M&amp;redir_token=AFhGtYUmwbM1V_PySqfshO8Svyd8MTU3NTY5NjgwMkAxNTc1NjEwNDAy&amp;q=https%3A%2F%2Fhexagon.pyrossoftware.com%2F" target="_blank" rel="nofollow noopener noreferrer">https://hexagon.pyrossoftware.com/
