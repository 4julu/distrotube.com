---
title: "Taking Into Account, Ep. 33 - GNOME, Skype, OpenJS, PureOS, Firefox Send, Poll Results"
image: images/thumbs/0365.jpg
date: Fri, 15 Mar 2019 23:11:25 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+33.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account:

<a href="https://www.youtube.com/watch?v=0shDTTJw60E&amp;t=44s">0:44 GNOME 3.32 is released and it brings a crop of new features and enhancements to the desktop. 

<a href="https://www.youtube.com/watch?v=0shDTTJw60E&amp;t=364s">6:04 Microsoft’s new Skype for Web no longer supports ChromeOS or Linux. 

<a href="https://www.youtube.com/watch?v=0shDTTJw60E&amp;t=586s">9:46 Linux Foundation announces the formation of OpenJS Foundation to support the JS community. 

<a href="https://www.youtube.com/watch?v=0shDTTJw60E&amp;t=745s">12:25 One operating system for two platforms?  PureOS: one Linux for both PCs and smartphones. 

<a href="https://www.youtube.com/watch?v=0shDTTJw60E&amp;t=1127s">18:47 Mozilla Firefox has announced the stable release of its free, encrypted file sharing service Firefox Send.  

<a href="https://www.youtube.com/watch?v=0shDTTJw60E&amp;t=1375s">22:55 I polled my viewers: what is your favorite text editor? 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F03%2Fbest-gnome-3-32-features&amp;redir_token=tr8RhZBhXfHmY7BkYBbe7FtrfbV8MTU1MzY0MTg4OEAxNTUzNTU1NDg4&amp;v=0shDTTJw60E&amp;event=video_description" target="_blank">https://www.omgubuntu.co.uk/2019/03/b...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fmspoweruser.com%2Fmicosofts-new-skype-for-web-no-longer-supports-chromebooks%2F&amp;redir_token=tr8RhZBhXfHmY7BkYBbe7FtrfbV8MTU1MzY0MTg4OEAxNTUzNTU1NDg4&amp;v=0shDTTJw60E&amp;event=video_description" target="_blank">https://mspoweruser.com/micosofts-new...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Ffossbytes.com%2Fthe-linux-foundation-openjs-foundation-javascript%2F&amp;redir_token=tr8RhZBhXfHmY7BkYBbe7FtrfbV8MTU1MzY0MTg4OEAxNTUzNTU1NDg4&amp;v=0shDTTJw60E&amp;event=video_description" target="_blank">https://fossbytes.com/the-linux-found...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fpureos-one-linux-for-both-pcs-and-smartphones%2F&amp;redir_token=tr8RhZBhXfHmY7BkYBbe7FtrfbV8MTU1MzY0MTg4OEAxNTUzNTU1NDg4&amp;v=0shDTTJw60E&amp;event=video_description" target="_blank">https://www.zdnet.com/article/pureos-...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fitsfoss.com%2Ffirefox-send%2F&amp;redir_token=tr8RhZBhXfHmY7BkYBbe7FtrfbV8MTU1MzY0MTg4OEAxNTUzNTU1NDg4&amp;v=0shDTTJw60E&amp;event=video_description" target="_blank">https://itsfoss.com/firefox-send/
