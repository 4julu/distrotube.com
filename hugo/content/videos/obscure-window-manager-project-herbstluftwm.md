---
title: "Obscure Window Manager Project - Herbstluftwm"
image: images/thumbs/0221.jpg
date: Wed, 30 May 2018 23:44:20 +0000
author: Derek Taylor
tags: ["Herbstluftwm", ""]
---

#### VIDEO

{{< amazon src="Obscure+Window+Manager+Project+-+Herbstluftwm.mp4" >}}
&nbsp;

#### SHOW NOTES

In this edition of the Obscure Window Manager Project, I take a look at herbstluftwm. It is a manual tiling window manager with some unique features. I've never used herbstluftwm before so this should be interesting. https://www.herbstluftwm.org/index.html
