---
title: "A First Look At Linux Mint 20 'Ulyana' Cinnamon"
image: images/thumbs/0656.jpg
date: 2020-06-29T12:23:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "Linux Mint"]
---

#### VIDEO

{{< amazon src="A+First+Look+At+Linux+Mint+20+Ulyana+Cinnamon.mp4" >}}
&nbsp;

#### SHOW NOTES

Linux Mint 20, codenamed "Ulyana," was recently released so I thought I would take a quick first look at Linux Mint 20 with the Cinnamon desktop environment.  Linux Mint 20 has made headlines recently due to their decision to try to block installation of snaps.

REFERENCED:
+ https://www.linuxmint.com/rel_ulyana_cinnamon_whatsnew.php