---
title: "Neptune 5.0 First Impression Install and Review"
image: images/thumbs/0144.jpg
date: Thu, 15 Mar 2018 21:36:33 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Neptune", "KDE"]
---

#### VIDEO

{{< amazon src="Neptune+5.0+First+Impression+Install+and+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this short video, I take a quick look at Neptune 5.0 "Refresh" which is a Debian-based distro that uses the KDE Plasma desktop. Based on Debian stable but using backported packages for Plasma and certain desktop apps, Neptune strikes a nice balance between stable and fresh. <a href="https://neptuneos.com/">https://neptuneos.com/</a>
