---
title: "The Window Manager Carousel Continues - Awesome Day One"
image: images/thumbs/0389.jpg
date: Mon, 06 May 2019 16:58:36 +0000
author: Derek Taylor
tags: ["tiling window managers", "awesome", "Live Stream"]
---

#### VIDEO

{{< amazon src="The+Window+Manager+Carousel+Continues+-+Awesome+Day+One-aWiKZnwOqhk.mp4" >}}
&nbsp;

#### SHOW NOTES

Feeling the urge again. That urge to Window Manager Hop! Been years since I lived in AwesomeWM but I remember liking it a lot back then. So I'm very much looking forward to spending some time in Awesome. Today, I'm going to start with a pretty stock Arco Awesome edition, and then I'm going to begin to hack it to suit my needs.
