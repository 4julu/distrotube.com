---
title: "Live Apr 6, 2018 - Installing BunsenLabs 'Helium' Beta 1"
image: images/thumbs/0170.jpg
date: Fri, 06 Apr 2018 22:22:10 +0000
author: Derek Taylor
tags: ["Distro Reviews", "BunsenLabs", "Openbox"]
---

#### VIDEO

{{< amazon src="Live+Apr+6%2C+2018+-+Installing+BunsenLabs+Helium+Beta+1.mp4" >}}
&nbsp;

#### SHOW NOTES

In this live stream, I'm going to install and review BunsenLabs latest beta release. BunsenLabs is a Debian-based distro that uses the Openbox window manager. I've reviewed BunsenLabs previously. I liked it but it's based on Debian Jessie (the old stable Debian) which is ancient. This new beta of BunsenLabs is based on Debian Stretch (the current stable Debian). 
