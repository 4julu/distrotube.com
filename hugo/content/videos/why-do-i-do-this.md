---
title: "Why Do I Do This?"
image: images/thumbs/0409.jpg
date: Sun, 02 Jun 2019 03:33:20 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Why+Do+I+Do+This.mp4" >}}
&nbsp;

#### SHOW NOTES

Why do I do what I do? I get this question often from friends,family and channel viewers. Why do I make these videos? What do I get out of all of this?

NOTE: Patrons were granted early access (one week) to this video. I want to thank my subscribers over on Patreon. You guys rock!
