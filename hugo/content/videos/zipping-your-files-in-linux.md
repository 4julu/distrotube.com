---
title: "Zipping Your Files In Linux"
image: images/thumbs/0358.jpg
date: Tue, 26 Feb 2019 23:04:46 +0000
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Zipping+Your+Files+In+Linux.mp4" >}}
&nbsp;

#### SHOW NOTES

Zipping files in Linux.  This video is a quick overview of using zip and related commands at the command line. 

📰 REFERENCED: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=zjI-A3EYzQ4&amp;redir_token=thzF99qYGU_NhaVSnfsSbB8f8558MTU1MzY0MTQ3NkAxNTUzNTU1MDc2&amp;q=https%3A%2F%2Flinux.die.net%2Fman%2F1%2Fzip" target="_blank">https://linux.die.net/man/1/zip
