---
title: "Command Line Audio Visualizer"
image: images/thumbs/0433.jpg
date: Sat, 17 Aug 2019 23:23:00 +0000
author: Derek Taylor
tags: ["command line", "terminal", "Audio"]
---

#### VIDEO

{{< amazon src="Command+Line+Audio+Visualizer.mp4" >}}
&nbsp;

#### SHOW NOTES

In this short video, I'm sharing a command line audio visualizer that I just came across. Works with your mpd music clients, desktop audio and microphones. Interfaces with ALSA and Pulseaudio. Has some nice customization options.


&nbsp;
#### REFERENCED: 
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgithub.com%2Fdpayne%2Fcli-visualizer&amp;redir_token=7OHClvzxO1GLcnZzfxZkrqcxwid8MTU3NzQ4OTA0MkAxNTc3NDAyNjQy&amp;v=LT4qUn5jInY&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://github.com/dpayne/cli-visualizer
