---
title: "DT Writes A Code Of Conduct"
image: images/thumbs/0520.jpg
date: 2020-01-20T12:22:40+06:00
author: Derek Taylor
tags: ["FOSS Philosophy", ""]
---

#### VIDEO

{{< amazon src="DT+Writes+A+Code+Of+Conduct.mp4" >}}
&nbsp;

#### SHOW NOTES

Too many Codes of Conduct are political in tone.  These codes are tearing about the free and open source software community.   Surely, it is possible to write a Code of Conduct that doesn't push some extremist political ideology.  At least, that's my goal here.

REFERENCED:
+ https://gitlab.com/dwt1/the-foss-code-of-conduct