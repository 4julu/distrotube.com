---
title: "Rolling Rhino Turns Ubuntu Into A Rolling Release"
image: images/thumbs/0661.jpg
date: 2020-07-06T12:23:40+06:00
author: Derek Taylor
tags: ["Ubuntu", ""]
---

#### VIDEO

{{< amazon src="Rolling+Rhino+Turns+Ubuntu+Into+A+Rolling+Release.mp4" >}}
&nbsp;

#### SHOW NOTES

You can now run Ubuntu as a "rolling release" by running "Rolling Rhino", a script that changes your apt sources to the devel series.  This works on all the official Ubuntu flavors.  Intended for experienced users.

REFERENCED:
+ https://github.com/wimpysworld/rolling-rhino - Rolling Rhino on GitHub