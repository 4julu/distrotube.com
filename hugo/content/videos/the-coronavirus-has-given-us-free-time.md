---
title: "The Coronavirus Has Given Us Free Time So Make Use Of It"
image: images/thumbs/0572.jpg
date: 2020-03-18T12:22:40+06:00
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="The+Coronavirus+Has+Given+Us+Free+Time+So+Make+Use+Of+It.mp4" >}}
&nbsp;

#### SHOW NOTES

The COVID-19 outbreak has caused a worldwide shutdown of non-essential services.  Many of us can't go to work, school, sporting events, movie theaters, etc.  So we are sitting at home for the next few weeks with a lot of extra time on our hands.  What could we be doing with that free time?