---
title: "Switching to Xmonad - Not Exactly Day One"
image: images/thumbs/0352.jpg
date: Sat, 16 Feb 2019 22:56:15 +0000
author: Derek Taylor
tags: ["tiling window managers", "Xmonad"]
---

#### VIDEO

{{< amazon src="Switching+to+Xmonad+Not+Exactly+Day+One.mp4" >}}
&nbsp;

#### SHOW NOTES

Today is (not exactly) Day One of me living in Xmonad, a tiling window manager written in Haskell. 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=f5JtgPALwUQ&amp;q=https%3A%2F%2Fxmonad.org%2F&amp;redir_token=SP4TkgXkgPZwrmO3uhicyaOjDrF8MTU1MzY0MDk5N0AxNTUzNTU0NTk3" target="_blank">https://xmonad.org/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=f5JtgPALwUQ&amp;q=https%3A%2F%2Fwiki.haskell.org%2FHaskell&amp;redir_token=SP4TkgXkgPZwrmO3uhicyaOjDrF8MTU1MzY0MDk5N0AxNTUzNTU0NTk3" target="_blank">https://wiki.haskell.org/Haskell

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=f5JtgPALwUQ&amp;q=https%3A%2F%2Fwww.haskell.org%2Fhoogle%2F&amp;redir_token=SP4TkgXkgPZwrmO3uhicyaOjDrF8MTU1MzY0MDk5N0AxNTUzNTU0NTk3" target="_blank">https://www.haskell.org/hoogle/
