---
title: "Distrohopped on the Laptop. Installed Solus!"
image: images/thumbs/0140.jpg
date: Mon, 12 Mar 2018 21:31:48 +0000
author: Derek Taylor
tags: ["Solus", ""]
---

#### VIDEO

{{< amazon src="Distrohopped+on+the+Laptop.+Installed+Solus!.mp4" >}}  
&nbsp;

#### SHOW NOTES

Just a quick video recorded on my laptop, a very cheap Toshiba Satellite that I paid around $350 for 3-4 years ago. It has an AMD A8 processor and was previously running Ubuntu 16.04. Today I installed Solus Budgie on it. 
