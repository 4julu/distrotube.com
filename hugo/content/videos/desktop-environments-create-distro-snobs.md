---
title: "Desktop Environments Create 'Distro Snobs'"
image: images/thumbs/0569.jpg
date: 2020-03-14T12:22:40+06:00
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Desktop+Environments+Create+Distro+Snobs.mp4" >}}
&nbsp;

#### SHOW NOTES

Distro snobs.  I don't like them.  You don't like them.  But let's talk about the reason so many of these distro fanboys exist.  It's because they use traditional desktop environments.