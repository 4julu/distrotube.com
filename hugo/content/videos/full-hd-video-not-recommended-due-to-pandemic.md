---
title: "Full HD Video Not Recommended Due To Pandemic"
image: images/thumbs/0578.jpg
date: 2020-03-25T12:22:40+06:00
author: Derek Taylor
tags: ["YouTube", ""]
---

#### VIDEO

{{< amazon src="Full+HD+Video+Not+Recommended+Due+To+Pandemic.mp4" >}}
&nbsp;

#### SHOW NOTES

YouTube is starting to default everyone to 480p video due to concerns surrounding the pandemic.  Hundreds of millions of people are stuck at home, and many of them are streaming video on YouTube.  Global Internet bandwidth is starting to become a concern, so YouTube is defaulting to 480p for the next month.

#### REFERENCED:
+ https://www.bloomberg.com/news/articles/2020-03-24/youtube-to-limit-video-quality-around-the-world-for-a-month