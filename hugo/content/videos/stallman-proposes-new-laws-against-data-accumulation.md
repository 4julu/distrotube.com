---
title: "Stallman Proposes New Laws Against Data Accumulation"
image: images/thumbs/0184.jpg
date: Sun, 22 Apr 2018 22:44:03 +0000
author: Derek Taylor
tags: ["Richard Stallman"]
---

#### VIDEO

{{< amazon src="Stallman+Proposes+New+Laws+Against+Data+Accumulation.mp4" >}}
&nbsp;

#### SHOW NOTES

Richard Stallman recently published an opinion piece proposing new laws be made to restrict the accumulation of personal data. This is article was written in the wake of the recent Facebook data scandal. I also discuss some of the reasons why Stallman has always been a vocal opponent of Facebook. I also contemplate moving away from Facebook myself, maybe to Diaspora? 

https://www.theguardian.com/commentisfree/2018/apr/03/facebook-abusing-data-law-privacy-big-tech-surveillance

https://stallman.org/facebook.html

https://diasporafoundation.org/
