---
title: "Off Topic - Getting To The Gym"
image: images/thumbs/0435.jpg
date: Sat, 24 Aug 2019 23:28:00 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Offtopic+Getting+To+The+Gym.mp4" >}}
&nbsp;

#### SHOW NOTES

An off topic video of me getting to the gym. I discuss why I started working out a few years ago and how it has helped me.
NOTE: This video was released one week early to my Patrons. Thank you, guys!
