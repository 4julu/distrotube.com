---
title: "Some Thoughts on YouTube, LBRY, Mastodon and Reddit"
image: images/thumbs/0536.jpg
date: 2020-02-05T12:22:40+06:00
author: Derek Taylor
tags: ["Social Media", "Facebook", "Mastodon", "Youtube", "LBRY"]
---

#### VIDEO

{{< amazon src="Some+Thoughts+on+YouTube%2C+LBRY%2C+Mastodon+and+Reddit.mp4" >}}
&nbsp;

#### SHOW NOTES

This is a rather long-winded rant about some social media questions and comments that I have received in recent weeks.  I will give some thoughts on YouTube, LBRY, Mastodon and Reddit.

REFERENCED:
+  https://lbry.tv/$/invite/@DistroTube:2 - DT on LBRY
+  https://joinmastodon.org/ - Join Mastodon!
+  https://www.reddit.com/r/DistroTube/ - DT on Reddit