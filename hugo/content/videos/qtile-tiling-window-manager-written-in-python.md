---
title: "Qtile - Tiling Window Manager Written in Python"
image: images/thumbs/0011.jpg
date: Sat, 14 Oct 2017 01:50:33 +0000
author: Derek Taylor
tags: ["Tiling Window Manager", "Qtile", "Python"]
---

#### VIDEO

{{< amazon src="Qtile+-+Tiling+Window+Manager+Written+in+Python.mp4" >}}  
&nbsp;

#### SHOW NOTES

I give a brief overview of the Qtile window manager. It is a tiling window manager that is written and configured in Python. Easy to install and easy to configure to your needs. Great community behind the project too. Try it out! Qtile - http://www.qtile.org/
