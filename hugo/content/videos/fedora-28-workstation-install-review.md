---
title: "Fedora 28 Workstation Install and Review"
image: images/thumbs/0193.jpg
date: Wed, 02 May 2018 22:54:50 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Fedora"]
---

#### VIDEO

{{< amazon src="Fedora+28+Workstation+Install+and+Review.mp4" >}}
&nbsp;

#### SHOW NOTES

Today I'm taking a quick look at the official release of Fedora 28 Workstation. I have previously reviewed Fedora 28 in its beta form about a month ago, and I also did video on how to make the GNOME desktop "tolerable" in Fedora 28. Check those out for further information. 
