---
title: "Are TODO Applications Hindering Your Productivity?"
image: images/thumbs/0659.jpg
date: 2020-07-03T12:23:40+06:00
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Are+Todo+Applications+Hindering+Your+Productivity.mp4" >}}
&nbsp;

#### SHOW NOTES

I often showcase productivity applications on video.  One of the most popular kinds of productivity apps is the todo app.  Although, I've showcased some of these on video, I personally never use them.  The reason is that I don't consider todo apps an efficient or a productive use of my time.