---
title: "I'm Replacing All Of My Programs...With Emacs"
image: images/thumbs/0649.jpg
date: 2020-06-19T12:23:40+06:00
author: Derek Taylor
tags: ["Emacs", ""]
---

#### VIDEO

{{< amazon src="I'm+Replacing+All+Of+My+Programs...With+Emacs.mp4" >}}
&nbsp;

#### SHOW NOTES

I've been using Emacs again and I have to admit, it is a rather charming piece of software.  And the more I use it, and the more plugins that I discover, the more I just want to replace all of my programs with Emacs alternatives.

REFERENCED:
+ https://github.com/hlissner/doom-emacs - Doom Emacs
