---
title: "Ubuntu Budgie 18.10 Replaces Mint 19 On My ThinkPad"
image: images/thumbs/0300.jpg
date: Tue, 23 Oct 2018 00:00:28 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Ubuntu Budgie"]
---

#### VIDEO

{{< amazon src="Ubuntu+Budgie+18.10+Replaces+Mint+19+On+My+ThinkPad.mp4" >}}
&nbsp;

#### SHOW NOTES

Time to distro-hop on my Lenovo ThinkPad.  This laptop had been running 
Mint 19 for a couple of months, which was great--no issues!  But I 
really want to check out Ubuntu Budgie on real hardware and see how it 
fares in the coming weeks/months.

<a href="https://www.youtube.com/redirect?redir_token=FkV6mW_qvLiVaHuP5XsAl9506XN8MTU1MzQ3MjA1MEAxNTUzMzg1NjUw&amp;v=Jlu9eMyZy8c&amp;q=https%3A%2F%2Fubuntubudgie.org%2F&amp;event=video_description" rel="noreferrer noopener" target="_blank">https://ubuntubudgie.org/
