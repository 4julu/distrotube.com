---
title: "What Is A TTY And How To Use It"
image: images/thumbs/0561.jpg
date: 2020-03-04T12:22:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="What+Is+A+TTY+And+How+To+Use+It.mp4" >}}
&nbsp;

#### SHOW NOTES

What is a tty and how do I use it?  I've been asked this question a few times in recent weeks, so I thought I would make a brief video discussing this.

REFERENCED:
+ https://en.wikipedia.org/wiki/TTY
