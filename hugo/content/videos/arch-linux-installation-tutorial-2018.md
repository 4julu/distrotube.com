---
title: "Arch Linux Installation Tutorial"
image: images/thumbs/0175.jpg
date: Thu, 12 Apr 2018 22:28:20 +0000
author: Derek Taylor
tags: ["Arch Linux", "Distro Reviews"]
---

#### VIDEO

{{< amazon src="Arch+Linux+Installation+Tutorial.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I run through a basic Arch Linux install inside a virtual machine. I simply read through the Arch Wiki and follow the instructions. Piece of cake! 
<a href="https://wiki.archlinux.org/index.php/installation_guide">https://wiki.archlinux.org/index.php/installation_guide</a>
