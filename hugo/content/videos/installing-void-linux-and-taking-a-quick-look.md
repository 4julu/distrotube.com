---
title: "Installing Void Linux And Taking A Quick Look Around"
image: images/thumbs/0308.jpg
date: Sat, 17 Nov 2018 00:28:43 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Void Linux"]
---

#### VIDEO

{{< amazon src="Installing+Void+Linux+And+Taking+A+Quick+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm going to run through the installation of Void Linux with the LXQt 
desktop environment.  I'm going to take a brief look around this very 
minimal Linux distribution.

<a href="https://www.youtube.com/redirect?redir_token=iGMrisbjtaf9rumhXGI-QwzxS1x8MTU1MzQ3Mzc1N0AxNTUzMzg3MzU3&amp;q=https%3A%2F%2Fvoidlinux.org%2F&amp;v=nWOeQoeO9Zo&amp;event=video_description" rel="noreferrer noopener" target="_blank">https://voidlinux.org/
