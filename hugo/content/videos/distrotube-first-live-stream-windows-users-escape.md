---
title: "DistroTube Live Stream Jan 24, 2018 - Windows Users Who Can't Escape"
image: images/thumbs/0096.jpg
date: Wed, 24 Jan 2018 01:22:51 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="DistroTube+Live+Stream+Jan+24%2C+2018+-+Windows+Users+Who+Cant+Escape.mp4" >}}
&nbsp;

#### SHOW NOTES

In tonight's live stream I talk to a few Windows users that know a little bit about Linux (they know it exists, at least) and why they cannot get away from Windows, at least not right now.     NOTE: Stream was killed by Youtube after a few minutes. My live streaming privileges were disabled by Youtube. Not sure what the issue was but I will appeal and see about streaming again in the near future.
