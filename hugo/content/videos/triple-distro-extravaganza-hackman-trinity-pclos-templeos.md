---
title: "Triple Distro Extravanganza - Hackman Linux, Trinity PCLOS, TempleOS"
image: images/thumbs/0283.jpg
date: Sat, 01 Sep 2018 19:07:06 +0000
author: Derek Taylor
tags: ["Distro Reviews", "TempleOS"]
---

#### VIDEO

{{< amazon src="Triple+Distro+Extravanganza+-+Hackman+Linux%2C+Trinity+PCLOS%2C+TempleOS.mp4" >}}
&nbsp;

#### SHOW NOTES

Today's live stream will be taking a brief look at three different  distros: Hackman Linux, Trinity PCLOS, TempleOS.  Hackman is the  creation of Ben Fitzpatrick and Matthew Moore.  Trinity PCLOS is the  creation of Presentarms.   TempleOS is the creation of Terry Davis. 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=5mz0mCtUBYnn73YpOZ5h6P4HeyJ8MTU1MzQ1NDQxOUAxNTUzMzY4MDE5&amp;v=dq9g4-tTiM4&amp;q=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fhackman-linux%2F&amp;event=video_description" target="_blank">https://sourceforge.net/projects/hack...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=5mz0mCtUBYnn73YpOZ5h6P4HeyJ8MTU1MzQ1NDQxOUAxNTUzMzY4MDE5&amp;v=dq9g4-tTiM4&amp;q=http%3A%2F%2Ftrinity.mypclinuxos.com%2F&amp;event=video_description" target="_blank">http://trinity.mypclinuxos.com/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=5mz0mCtUBYnn73YpOZ5h6P4HeyJ8MTU1MzQ1NDQxOUAxNTUzMzY4MDE5&amp;v=dq9g4-tTiM4&amp;q=http%3A%2F%2Fwww.templeos.org%2F&amp;event=video_description" target="_blank">http://www.templeos.org/
