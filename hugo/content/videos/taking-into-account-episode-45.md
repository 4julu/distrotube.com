---
title: "Taking Into Account, Ep. 45 (LIVE)"
image: images/thumbs/0413.jpg
date: Thu, 13 Jun 2019 05:21:00 +0000
author: Derek Taylor
tags: ["Taking Into Account", "Live Stream"]
---

#### VIDEO

{{< amazon src="Taking+Into+Account%2C+Ep.+45+(LIVE)-nBhriZ4xIu8.mp4" >}}
&nbsp;

#### SHOW NOTES

This week's edition of Taking Into Account...LIVE!


&nbsp;
#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?redir_token=h3MzJtB7vi9QKLFzOY3-yG5v8FJ8MTU3NTY5NjI2NEAxNTc1NjA5ODY0&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fthe-linux-desktops-last-best-shot%2F&amp;event=video_description&amp;v=nBhriZ4xIu8" target="_blank" rel="nofollow noopener noreferrer">https://www.zdnet.com/article/the-lin...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?redir_token=h3MzJtB7vi9QKLFzOY3-yG5v8FJ8MTU3NTY5NjI2NEAxNTc1NjA5ODY0&amp;q=https%3A%2F%2Fwww.linuxjournal.com%2Fcontent%2Ffacebook-not-microsoft-main-threat-open-source&amp;event=video_description&amp;v=nBhriZ4xIu8" target="_blank" rel="nofollow noopener noreferrer">https://www.linuxjournal.com/content/...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?redir_token=h3MzJtB7vi9QKLFzOY3-yG5v8FJ8MTU3NTY5NjI2NEAxNTc1NjA5ODY0&amp;q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F06%2Fcern-ditch-microsoft-open-source-malt&amp;event=video_description&amp;v=nBhriZ4xIu8" target="_blank" rel="nofollow noopener noreferrer">https://www.omgubuntu.co.uk/2019/06/c...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?redir_token=h3MzJtB7vi9QKLFzOY3-yG5v8FJ8MTU3NTY5NjI2NEAxNTc1NjA5ODY0&amp;q=https%3A%2F%2Farstechnica.com%2Finformation-technology%2F2019%2F06%2Fif-you-havent-patched-vim-or-neovim-text-editors-you-really-really-should%2F%3Fcomments%3D1&amp;event=video_description&amp;v=nBhriZ4xIu8" target="_blank" rel="nofollow noopener noreferrer">https://arstechnica.com/information-t...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?redir_token=h3MzJtB7vi9QKLFzOY3-yG5v8FJ8MTU3NTY5NjI2NEAxNTc1NjA5ODY0&amp;q=https%3A%2F%2Fbitcannon.net%2Fpost%2Fpro-desktop%2F&amp;event=video_description&amp;v=nBhriZ4xIu8" target="_blank" rel="nofollow noopener noreferrer">https://bitcannon.net/post/pro-desktop/
