---
title: "GhostBSD First Impression Install and Review"
image: images/thumbs/0153.jpg
date: Wed, 21 Mar 2018 21:46:20 +0000
author: Derek Taylor
tags: ["Distro Reviews", "BSD", "GhostBSD"]
---

#### VIDEO

{{< amazon src="GhostBSD+First+Impression+Install+and+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this first impression install and review, I take a look at GhostBSD. I've only reviewed one other BSD distro (TrueOS) on this channel and I hated it. I've had a lot of viewers tell me that I needed to look at GhostBSD, so that's what I did. <a href="http://ghostbsd.org/">http://ghostbsd.org/</a>
