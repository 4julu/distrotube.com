---
title: "Getting Started With Xmonad"
image: images/thumbs/0622.jpg
date: 2020-05-14T12:23:40+06:00
author: Derek Taylor
tags: ["Tiling Window Managers", "xmonad"]
---

#### VIDEO

{{< amazon src="Getting+Started+With+Xmonad.mp4" >}}
&nbsp;

#### SHOW NOTES

In this rather lengthy video, I go over the basics of getting started with Xmonad.  I install Xmonad on Ubuntu 20.04 and show some of the basics as far as configuration.   I also show you how to use the Haskell documentation.  And I briefly show you how to install and configure Xmobar. 